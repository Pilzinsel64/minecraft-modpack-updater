<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1

    Inherits Telerik.WinControls.UI.RadForm

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel2 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel3 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel_Status = New Telerik.WinControls.UI.RadLabel()
        Me.RadTextBoxControl_MinecraftProfileFolder = New Telerik.WinControls.UI.RadTextBoxControl()
        Me.RadTextBoxControl_ModpackConfig = New Telerik.WinControls.UI.RadTextBoxControl()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RadButton_Install = New Telerik.WinControls.UI.RadButton()
        Me.RadButton_CheckForUpdates = New Telerik.WinControls.UI.RadButton()
        Me.RadButton_EditModpackConfig = New Telerik.WinControls.UI.RadButton()
        Me.RadButton_SearchModpackConfig = New Telerik.WinControls.UI.RadButton()
        Me.RadButton_SearchMinecraftProfileFolder = New Telerik.WinControls.UI.RadButton()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel_Status, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadTextBoxControl_MinecraftProfileFolder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadTextBoxControl_ModpackConfig, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.RadButton_Install, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton_CheckForUpdates, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton_EditModpackConfig, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton_SearchModpackConfig, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton_SearchMinecraftProfileFolder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadLabel1
        '
        Me.RadLabel1.Location = New System.Drawing.Point(3, 5)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(135, 19)
        Me.RadLabel1.TabIndex = 0
        Me.RadLabel1.Text = "Minecraft profile folder:"
        '
        'RadLabel2
        '
        Me.RadLabel2.Location = New System.Drawing.Point(3, 63)
        Me.RadLabel2.Name = "RadLabel2"
        Me.RadLabel2.Size = New System.Drawing.Size(98, 19)
        Me.RadLabel2.TabIndex = 1
        Me.RadLabel2.Text = "Modpack config:"
        '
        'RadLabel3
        '
        Me.RadLabel3.Location = New System.Drawing.Point(3, 121)
        Me.RadLabel3.Name = "RadLabel3"
        Me.RadLabel3.Size = New System.Drawing.Size(43, 19)
        Me.RadLabel3.TabIndex = 2
        Me.RadLabel3.Text = "Status:"
        '
        'RadLabel_Status
        '
        Me.RadLabel_Status.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadLabel_Status.AutoSize = False
        Me.RadLabel_Status.Location = New System.Drawing.Point(144, 119)
        Me.RadLabel_Status.Name = "RadLabel_Status"
        Me.RadLabel_Status.Size = New System.Drawing.Size(273, 22)
        Me.RadLabel_Status.TabIndex = 3
        Me.RadLabel_Status.Text = "-"
        Me.RadLabel_Status.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        '
        'RadTextBoxControl_MinecraftProfileFolder
        '
        Me.RadTextBoxControl_MinecraftProfileFolder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadTextBoxControl_MinecraftProfileFolder.IsReadOnly = True
        Me.RadTextBoxControl_MinecraftProfileFolder.Location = New System.Drawing.Point(144, 3)
        Me.RadTextBoxControl_MinecraftProfileFolder.Name = "RadTextBoxControl_MinecraftProfileFolder"
        Me.RadTextBoxControl_MinecraftProfileFolder.NullText = "No file loaded!"
        Me.RadTextBoxControl_MinecraftProfileFolder.Size = New System.Drawing.Size(273, 22)
        Me.RadTextBoxControl_MinecraftProfileFolder.TabIndex = 4
        '
        'RadTextBoxControl_ModpackConfig
        '
        Me.RadTextBoxControl_ModpackConfig.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadTextBoxControl_ModpackConfig.IsReadOnly = True
        Me.RadTextBoxControl_ModpackConfig.Location = New System.Drawing.Point(144, 61)
        Me.RadTextBoxControl_ModpackConfig.Name = "RadTextBoxControl_ModpackConfig"
        Me.RadTextBoxControl_ModpackConfig.NullText = "No file loaded!"
        Me.RadTextBoxControl_ModpackConfig.Size = New System.Drawing.Size(273, 22)
        Me.RadTextBoxControl_ModpackConfig.TabIndex = 5
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.RadButton_Install)
        Me.Panel1.Controls.Add(Me.RadButton_CheckForUpdates)
        Me.Panel1.Controls.Add(Me.RadButton_EditModpackConfig)
        Me.Panel1.Controls.Add(Me.RadButton_SearchModpackConfig)
        Me.Panel1.Controls.Add(Me.RadButton_SearchMinecraftProfileFolder)
        Me.Panel1.Controls.Add(Me.RadLabel1)
        Me.Panel1.Controls.Add(Me.RadTextBoxControl_ModpackConfig)
        Me.Panel1.Controls.Add(Me.RadLabel2)
        Me.Panel1.Controls.Add(Me.RadTextBoxControl_MinecraftProfileFolder)
        Me.Panel1.Controls.Add(Me.RadLabel3)
        Me.Panel1.Controls.Add(Me.RadLabel_Status)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(420, 176)
        Me.Panel1.TabIndex = 6
        '
        'RadButton_Install
        '
        Me.RadButton_Install.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadButton_Install.Image = Global.ModpackUpdater.My.Resources.MySymbols.icons8_software_installer_16px
        Me.RadButton_Install.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.RadButton_Install.Location = New System.Drawing.Point(337, 149)
        Me.RadButton_Install.Name = "RadButton_Install"
        Me.RadButton_Install.Size = New System.Drawing.Size(80, 24)
        Me.RadButton_Install.TabIndex = 10
        Me.RadButton_Install.Text = "Install"
        Me.RadButton_Install.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft
        Me.RadButton_Install.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        '
        'RadButton_CheckForUpdates
        '
        Me.RadButton_CheckForUpdates.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadButton_CheckForUpdates.Image = Global.ModpackUpdater.My.Resources.MySymbols.icons8_update_16px
        Me.RadButton_CheckForUpdates.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.RadButton_CheckForUpdates.Location = New System.Drawing.Point(191, 149)
        Me.RadButton_CheckForUpdates.Name = "RadButton_CheckForUpdates"
        Me.RadButton_CheckForUpdates.Size = New System.Drawing.Size(140, 24)
        Me.RadButton_CheckForUpdates.TabIndex = 0
        Me.RadButton_CheckForUpdates.Text = "Check for Updates"
        Me.RadButton_CheckForUpdates.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft
        Me.RadButton_CheckForUpdates.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        '
        'RadButton_EditModpackConfig
        '
        Me.RadButton_EditModpackConfig.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.RadButton_EditModpackConfig.Image = Global.ModpackUpdater.My.Resources.MySymbols.icons8_wrench_16px
        Me.RadButton_EditModpackConfig.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.RadButton_EditModpackConfig.Location = New System.Drawing.Point(3, 149)
        Me.RadButton_EditModpackConfig.Name = "RadButton_EditModpackConfig"
        Me.RadButton_EditModpackConfig.Size = New System.Drawing.Size(150, 24)
        Me.RadButton_EditModpackConfig.TabIndex = 8
        Me.RadButton_EditModpackConfig.Text = "Edit modpack config"
        Me.RadButton_EditModpackConfig.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft
        Me.RadButton_EditModpackConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        '
        'RadButton_SearchModpackConfig
        '
        Me.RadButton_SearchModpackConfig.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadButton_SearchModpackConfig.Image = CType(resources.GetObject("RadButton_SearchModpackConfig.Image"), System.Drawing.Image)
        Me.RadButton_SearchModpackConfig.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.RadButton_SearchModpackConfig.Location = New System.Drawing.Point(327, 89)
        Me.RadButton_SearchModpackConfig.Name = "RadButton_SearchModpackConfig"
        Me.RadButton_SearchModpackConfig.Size = New System.Drawing.Size(90, 24)
        Me.RadButton_SearchModpackConfig.TabIndex = 7
        Me.RadButton_SearchModpackConfig.Text = "Search"
        Me.RadButton_SearchModpackConfig.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft
        Me.RadButton_SearchModpackConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        '
        'RadButton_SearchMinecraftProfileFolder
        '
        Me.RadButton_SearchMinecraftProfileFolder.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadButton_SearchMinecraftProfileFolder.Image = CType(resources.GetObject("RadButton_SearchMinecraftProfileFolder.Image"), System.Drawing.Image)
        Me.RadButton_SearchMinecraftProfileFolder.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.RadButton_SearchMinecraftProfileFolder.Location = New System.Drawing.Point(327, 31)
        Me.RadButton_SearchMinecraftProfileFolder.Name = "RadButton_SearchMinecraftProfileFolder"
        Me.RadButton_SearchMinecraftProfileFolder.Size = New System.Drawing.Size(90, 24)
        Me.RadButton_SearchMinecraftProfileFolder.TabIndex = 6
        Me.RadButton_SearchMinecraftProfileFolder.Text = "Search"
        Me.RadButton_SearchMinecraftProfileFolder.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft
        Me.RadButton_SearchMinecraftProfileFolder.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(7, 15)
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(420, 176)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        '
        '
        '
        Me.RootElement.ApplyShapeToControl = True
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Minecraft Modpack Updater"
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel_Status, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadTextBoxControl_MinecraftProfileFolder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadTextBoxControl_ModpackConfig, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.RadButton_Install, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton_CheckForUpdates, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton_EditModpackConfig, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton_SearchModpackConfig, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton_SearchMinecraftProfileFolder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel2 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel3 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel_Status As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadTextBoxControl_MinecraftProfileFolder As Telerik.WinControls.UI.RadTextBoxControl
    Friend WithEvents RadTextBoxControl_ModpackConfig As Telerik.WinControls.UI.RadTextBoxControl
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents RadButton_Install As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton_CheckForUpdates As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton_EditModpackConfig As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton_SearchModpackConfig As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton_SearchMinecraftProfileFolder As Telerik.WinControls.UI.RadButton
End Class
