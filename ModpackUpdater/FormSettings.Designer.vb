﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormSettings
    Inherits Telerik.WinControls.UI.RadForm

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormSettings))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RadTextBoxControl_WebdavPassword = New Telerik.WinControls.UI.RadTextBoxControl()
        Me.RadTextBoxControl_WebdavUsername = New Telerik.WinControls.UI.RadTextBoxControl()
        Me.RadTextBoxControl_WebdavURL = New Telerik.WinControls.UI.RadTextBoxControl()
        Me.RadButton_SaveAndClose = New Telerik.WinControls.UI.RadButton()
        Me.RadButton_SaveAs = New Telerik.WinControls.UI.RadButton()
        Me.RadLabel3 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel2 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.RadTextBoxControl_UpdateUrl = New Telerik.WinControls.UI.RadTextBoxControl()
        Me.RadLabel4 = New Telerik.WinControls.UI.RadLabel()
        Me.Panel1.SuspendLayout()
        CType(Me.RadTextBoxControl_WebdavPassword, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadTextBoxControl_WebdavUsername, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadTextBoxControl_WebdavURL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton_SaveAndClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton_SaveAs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadTextBoxControl_UpdateUrl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.RadTextBoxControl_UpdateUrl)
        Me.Panel1.Controls.Add(Me.RadLabel4)
        Me.Panel1.Controls.Add(Me.RadTextBoxControl_WebdavPassword)
        Me.Panel1.Controls.Add(Me.RadTextBoxControl_WebdavUsername)
        Me.Panel1.Controls.Add(Me.RadTextBoxControl_WebdavURL)
        Me.Panel1.Controls.Add(Me.RadButton_SaveAndClose)
        Me.Panel1.Controls.Add(Me.RadButton_SaveAs)
        Me.Panel1.Controls.Add(Me.RadLabel3)
        Me.Panel1.Controls.Add(Me.RadLabel2)
        Me.Panel1.Controls.Add(Me.RadLabel1)
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Name = "Panel1"
        '
        'RadTextBoxControl_WebdavPassword
        '
        resources.ApplyResources(Me.RadTextBoxControl_WebdavPassword, "RadTextBoxControl_WebdavPassword")
        Me.RadTextBoxControl_WebdavPassword.Name = "RadTextBoxControl_WebdavPassword"
        '
        'RadTextBoxControl_WebdavUsername
        '
        resources.ApplyResources(Me.RadTextBoxControl_WebdavUsername, "RadTextBoxControl_WebdavUsername")
        Me.RadTextBoxControl_WebdavUsername.Name = "RadTextBoxControl_WebdavUsername"
        '
        'RadTextBoxControl_WebdavURL
        '
        resources.ApplyResources(Me.RadTextBoxControl_WebdavURL, "RadTextBoxControl_WebdavURL")
        Me.RadTextBoxControl_WebdavURL.Name = "RadTextBoxControl_WebdavURL"
        '
        'RadButton_SaveAndClose
        '
        resources.ApplyResources(Me.RadButton_SaveAndClose, "RadButton_SaveAndClose")
        Me.RadButton_SaveAndClose.Image = Global.ModpackUpdater.My.Resources.MySymbols.icons8_save_16px
        Me.RadButton_SaveAndClose.Name = "RadButton_SaveAndClose"
        '
        'RadButton_SaveAs
        '
        resources.ApplyResources(Me.RadButton_SaveAs, "RadButton_SaveAs")
        Me.RadButton_SaveAs.Image = Global.ModpackUpdater.My.Resources.MySymbols.icons8_opened_folder_16px
        Me.RadButton_SaveAs.Name = "RadButton_SaveAs"
        '
        'RadLabel3
        '
        resources.ApplyResources(Me.RadLabel3, "RadLabel3")
        Me.RadLabel3.Name = "RadLabel3"
        '
        'RadLabel2
        '
        resources.ApplyResources(Me.RadLabel2, "RadLabel2")
        Me.RadLabel2.Name = "RadLabel2"
        '
        'RadLabel1
        '
        resources.ApplyResources(Me.RadLabel1, "RadLabel1")
        Me.RadLabel1.Name = "RadLabel1"
        '
        'RadTextBoxControl_UpdateUrl
        '
        resources.ApplyResources(Me.RadTextBoxControl_UpdateUrl, "RadTextBoxControl_UpdateUrl")
        Me.RadTextBoxControl_UpdateUrl.Name = "RadTextBoxControl_UpdateUrl"
        '
        'RadLabel4
        '
        resources.ApplyResources(Me.RadLabel4, "RadLabel4")
        Me.RadLabel4.Name = "RadLabel4"
        '
        'FormSettings
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.Name = "FormSettings"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.RadTextBoxControl_WebdavPassword, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadTextBoxControl_WebdavUsername, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadTextBoxControl_WebdavURL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton_SaveAndClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton_SaveAs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadTextBoxControl_UpdateUrl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadTextBoxControl_WebdavPassword As Telerik.WinControls.UI.RadTextBoxControl
    Friend WithEvents RadTextBoxControl_WebdavUsername As Telerik.WinControls.UI.RadTextBoxControl
    Friend WithEvents RadTextBoxControl_WebdavURL As Telerik.WinControls.UI.RadTextBoxControl
    Friend WithEvents RadButton_SaveAndClose As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton_SaveAs As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadLabel3 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel2 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadTextBoxControl_UpdateUrl As Telerik.WinControls.UI.RadTextBoxControl
    Friend WithEvents RadLabel4 As Telerik.WinControls.UI.RadLabel
End Class
