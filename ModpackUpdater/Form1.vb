Imports System.IO

Imports ModpackUpdater.Manager

Imports ModpackUpdater.Model

Imports ModpackUpdater.My.Resources

Imports Telerik.WinControls
Imports Telerik.WinControls.UI

Public Class Form1

    Private updateConfig As New UpdateConfig
    Private currentUpdating As Boolean = False
    Private lastUpdateCheckResult As UpdateCheckResult = Nothing

    Public Sub New()
        InitializeComponent()
        Text = $"{Text} (v{Application.ProductVersion})"
    End Sub

    Private Function IsMinecaftProfileLoaded() As Boolean
        Return Not String.IsNullOrEmpty(GetMinecraftProfilePath)
    End Function

    Private Function GetMinecraftProfilePath() As String
        Return RadTextBoxControl_MinecraftProfileFolder.Text.Trim
    End Function

    Private Function IsUpdateConfigLoaded() As Boolean
        Return Not String.IsNullOrEmpty(GetUpdateconfigPath)
    End Function

    Private Function GetUpdateconfigPath() As String
        Return RadTextBoxControl_ModpackConfig.Text.Trim
    End Function

    Private Function CheckStatus() As Boolean
        If Not IsMinecaftProfileLoaded() OrElse Not MinecraftProfileChecker.CheckProfile(GetMinecraftProfilePath) Then
            SetStatus(LangRes.StatusText_MinecraftProfileWarning, MySymbols.icons8_general_warning_sign_16px)
            CheckStatus = False
        ElseIf Not IsUpdateConfigLoaded() Then
            SetStatus(LangRes.StatusText_MinecraftProfileWarning, MySymbols.icons8_general_warning_sign_16px)
            CheckStatus = False
        Else
            CheckStatus = True
        End If
    End Function

    Private Sub SetStatus(statusText As String, image As Image)
        RadLabel_Status.Text = statusText
        RadLabel_Status.Image = image
    End Sub

    Private Sub ClearStatus()
        RadLabel_Status.Text = "-"
        RadLabel_Status.Image = Nothing
    End Sub

    Private Sub LoadMinecraftProfile(folderPath As String)
        RadTextBoxControl_MinecraftProfileFolder.Text = folderPath

        If IsUpdateConfigLoaded() Then
            RadButton_CheckForUpdates.PerformClick()
        Else
            ClearStatus()
        End If
    End Sub

    Private Sub LoadUpdateConfigFile(filePath As String)
        RadTextBoxControl_ModpackConfig.Text = filePath

        If IsUpdateConfigLoaded() Then
            updateConfig = UpdateConfig.LoadFromFile(filePath)
        End If

        If IsMinecaftProfileLoaded() Then
            RadButton_CheckForUpdates.PerformClick()
        Else
            ClearStatus()
        End If
    End Sub

    Private Async Function ExecuteUpdate(doInstall As Boolean) As Task
        Dim updater As New UpdateInstaller(updateConfig, GetMinecraftProfilePath)
        AddHandler updater.InstallProgessUpdated, AddressOf Update_InstallProgessUpdated
        AddHandler updater.CheckingProgressUpdated, AddressOf Updated_CheckingProgresssUpdated

        'Check only if not pressed "install", not really needed otherwise.
        If lastUpdateCheckResult Is Nothing OrElse Not doInstall Then
            SetStatus(LangRes.StatusText_CheckingForUpdates, MySymbols.icons8_update_16px)

            Try
                lastUpdateCheckResult = Await updater.CheckForUpdates(Not AppConfig.Instance.AllowRemoveLocalFiles)
            Catch
                SetStatus(LangRes.StatusText_ErrorWhileUpdateCheckOrUpdate, MySymbols.icons8_delete_16px)
            Finally
            End Try
        End If

        If lastUpdateCheckResult Is Nothing OrElse lastUpdateCheckResult.HasError Then
            SetStatus(LangRes.StatusText_ErrorWhileUpdateCheckOrUpdate, MySymbols.icons8_delete_16px)
        ElseIf lastUpdateCheckResult.HasUpdates Then
            If doInstall Then
                SetStatus(LangRes.StatusText_Installing, MySymbols.icons8_software_installer_16px)
                currentUpdating = True

                Try
                    If Await updater.InstallUpdates(lastUpdateCheckResult) Then
                        lastUpdateCheckResult = Nothing 'Reset last update check, a new one would be needed now.
                        SetStatus(LangRes.StatusTest_EverythingOk, MySymbols.icons8_checkmark_16px)
                    Else
                        SetStatus(LangRes.StatusText_ErrorWhileUpdateCheckOrUpdate, MySymbols.icons8_delete_16px)
                    End If
                Catch
                    SetStatus(LangRes.StatusText_ErrorWhileUpdateCheckOrUpdate, MySymbols.icons8_delete_16px)
                Finally
                    currentUpdating = False
                End Try
            Else
                SetStatus(LangRes.StatusText_UpdateAvailable, MySymbols.icons8_software_installer_16px)
            End If
        Else
            SetStatus(LangRes.StatusTest_EverythingOk, MySymbols.icons8_checkmark_16px)
        End If
    End Function

    Private Sub Update_InstallProgessUpdated(result As UpdateCheckResult, processedSyncs As Integer)
        Dim actionCount = If(result.IsLegacy, result.SyncFiles.Count, result.UpdateActions.Count)
        SetStatus(Math.Round(processedSyncs / actionCount * 100, 1) & "%", MySymbols.icons8_software_installer_16px)
    End Sub

    Private Sub Updated_CheckingProgresssUpdated(toCheck As Integer, processed As Integer)
        SetStatus(Math.Round(processed / toCheck * 100, 1) & "%", MySymbols.icons8_update_16px)
    End Sub

    Private Sub ButtonX_SearchMinecraftProfile_Click(sender As Object, e As EventArgs) Handles RadButton_SearchMinecraftProfileFolder.Click
        Dim ofd As New RadOpenFolderDialog

        If ofd.ShowDialog(Me) = DialogResult.OK Then
            LoadMinecraftProfile(ofd.FileName)
        End If
    End Sub

    Private Sub ButtonX_SearchUpdateConfig_Click(sender As Object, e As EventArgs) Handles RadButton_SearchModpackConfig.Click
        Dim ofd As New RadOpenFileDialog
        ofd.Filter = FiledialogFilters.JSON_Display & "|" & FiledialogFilters.JSON_Filter

        If ofd.ShowDialog(Me) = DialogResult.OK Then
            LoadUpdateConfigFile(ofd.FileName)
        End If
    End Sub

    Private Sub ButtonX_EditUpdateConfig_Click(sender As Object, e As EventArgs) Handles RadButton_EditModpackConfig.Click
        Dim frm As New FormSettings(updateConfig)
        frm.ShowDialog(Me)
    End Sub

    Private Async Sub ButtonX_CheckForUpdates_Click(sender As Object, e As EventArgs) Handles RadButton_CheckForUpdates.Click
        ClearStatus()
        If CheckStatus() Then
            Await ExecuteUpdate(False)
        End If
    End Sub

    Private Async Sub ButtonX_StartUpdate_Click(sender As Object, e As EventArgs) Handles RadButton_Install.Click
        If Not currentUpdating Then
            ClearStatus()
            If CheckStatus() Then
                Await ExecuteUpdate(True)
            End If
        End If
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        AppConfig.Instance.LastMinecraftProfilePath = RadTextBoxControl_MinecraftProfileFolder.Text
        AppConfig.Instance.LastConfigFilePath = RadTextBoxControl_ModpackConfig.Text
        AppConfig.Instance.SaveConfig()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Directory.Exists(AppConfig.Instance.LastMinecraftProfilePath) Then
            LoadMinecraftProfile(AppConfig.Instance.LastMinecraftProfilePath)
        End If
        If File.Exists(AppConfig.Instance.LastConfigFilePath) Then
            LoadUpdateConfigFile(AppConfig.Instance.LastConfigFilePath)
        End If
    End Sub

    Private Async Sub Form1_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If Await AppUpdater.Check AndAlso RadMessageBox.Show(LangRes.MsgBox_UpdateAvailable, LangRes.MsgBox_UpdateAvailable_Title, MessageBoxButtons.YesNo, RadMessageIcon.Info) = DialogResult.Yes Then
            SetStatus(LangRes.StatusText_InstallingAppUpdate, MySymbols.icons8_software_installer_16px)
            Enabled = False
            Await AppUpdater.Install()
            Application.Restart()
        End If
    End Sub
End Class
