﻿Imports System.IO
Imports System.Net
Imports System.Net.Http

Imports WebDav

Public Class AppUpdater

    Private Const WEBDAV_URL As String = "https://cloud.pilzinsel64.de/public.php/webdav"
    Private Const WEBDAV_KEY As String = "z4dGicDYmG2neG9"
    Private Const DOWNLOAD_URL As String = "https://cloud.pilzinsel64.de/s/z4dGicDYmG2neG9/download"

    Public Shared Async Function Check() As Task(Of Boolean)
        Dim appFileName = Pilz.IO.Extensions.GetExecutablePath()
        Dim hasUpdate As Boolean = False
        Dim params As New WebDavClientParams With {
            .BaseAddress = New Uri(WEBDAV_URL),
            .Credentials = New NetworkCredential(WEBDAV_KEY, String.Empty)
        }

        Try
            Using client As New WebDavClient(params)
                Dim result = Await client.Propfind(String.Empty)

                If result.IsSuccessful AndAlso result.Resources.Count <> 0 Then
                    Dim resource = result.Resources(0)
                    Dim appModificationDate = File.GetLastWriteTimeUtc(appFileName)
                    Dim remoteModificationDate = resource.LastModifiedDate?.ToUniversalTime

                    If remoteModificationDate > appModificationDate Then
                        hasUpdate = True
                    End If
                End If
            End Using
        Catch ex As Exception
        End Try

        Return hasUpdate
    End Function

    Public Shared Async Function Install() As Task
        Dim client As New HttpClient
        Dim tempFileName = Path.GetTempFileName
        Dim appFileName = Pilz.IO.Extensions.GetExecutablePath()
        Dim oldFileName = appFileName & ".old"

        'Delete old file
        Try
            File.Delete(oldFileName)
        Catch ex As Exception
        End Try

        'Download the new file
        Using tempFileStream As New FileStream(tempFileName, FileMode.Create, FileAccess.ReadWrite)
            Dim downloadStream As Stream = Nothing
            Try
                downloadStream = Await client.GetStreamAsync(DOWNLOAD_URL)
                Await downloadStream.CopyToAsync(tempFileStream)
            Catch
            Finally
                downloadStream?.Dispose()
            End Try
        End Using

        'Replace current application file with new file
        File.Move(appFileName, oldFileName, True)
        File.Move(tempFileName, appFileName)
    End Function

End Class
