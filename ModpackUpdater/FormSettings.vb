﻿Imports ModpackUpdater.Model
Imports ModpackUpdater.My.Resources

Imports Telerik.WinControls.UI

Public Class FormSettings

    Private ReadOnly updateConfig As UpdateConfig

    Public Sub New(updateConfig As UpdateConfig)
        InitializeComponent()
        Me.updateConfig = updateConfig
    End Sub

    Private Sub LoadConfig()
        RadTextBoxControl_WebdavURL.Text = updateConfig.WebdavURL
        RadTextBoxControl_WebdavUsername.Text = updateConfig.WebdavUsername
        RadTextBoxControl_WebdavPassword.Text = updateConfig.WebdavPassword
        RadTextBoxControl_UpdateUrl.Text = updateConfig.UpdateUrl
    End Sub

    Private Sub SaveConfig()
        updateConfig.WebdavURL = RadTextBoxControl_WebdavURL.Text.Trim
        updateConfig.WebdavUsername = RadTextBoxControl_WebdavUsername.Text.Trim
        updateConfig.WebdavPassword = RadTextBoxControl_WebdavPassword.Text
        updateConfig.UpdateUrl = RadTextBoxControl_UpdateUrl.Text
    End Sub

    Private Sub ButtonX_SaveAs_Click(sender As Object, e As EventArgs) Handles RadButton_SaveAs.Click
        Dim ofd As New RadSaveFileDialog
        ofd.Filter = FiledialogFilters.JSON_Display & "|" & FiledialogFilters.JSON_Filter

        If ofd.ShowDialog(Me) = DialogResult.OK Then
            updateConfig.SaveToFile(ofd.FileName)
        End If
    End Sub

    Private Sub ButtonX_SaveAndClose_Click(sender As Object, e As EventArgs) Handles RadButton_SaveAndClose.Click
        SaveConfig()
        Close()
    End Sub

    Private Sub FormSettings_Load(sender As Object, e As EventArgs) Handles Me.Load
        LoadConfig()
    End Sub

End Class
