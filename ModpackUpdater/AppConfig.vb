Imports System.IO

Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class AppConfig

    Public Property LastMinecraftProfilePath As String
    Public Property LastConfigFilePath As String
    Public Property KeepLocalFiles As New List(Of String)
    Public Property AllowRemoveLocalFiles As Boolean

    Public Sub Reset()
        KeepLocalFiles.Clear()
        KeepLocalFiles.Add("OptiFine_1.7.10_HD_U_E7.jar")
        AllowRemoveLocalFiles = False
    End Sub

    Public Shared ReadOnly Property Instance As AppConfig
        Get
            Static myInstance As AppConfig = Nothing

            If myInstance Is Nothing Then
                If File.Exists(SettingsPath) Then
                    myInstance = LoadConfig(SettingsPath)
                Else
                    myInstance = New AppConfig
                    myInstance.Reset()
                End If
            End If

            Return myInstance
        End Get
    End Property

    Private Shared ReadOnly Property SettingsPath As string
        Get
            Static myPath As String = String.Empty
            Const AppDataDirectoryName As String = "MinecraftModpackUpdater"
            Const SettingsFileName As String = "Settings.json"

            If String.IsNullOrEmpty(myPath) Then
                myPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), AppDataDirectoryName)
                Directory.CreateDirectory(myPath)
                myPath = Path.Combine(myPath, SettingsFileName)
            End If

            Return myPath
        End Get
    End Property

    Public Sub SaveConfig()
        File.WriteAllText(SettingsPath, JObject.FromObject(Me).ToString)
    End Sub

    Private Shared Function LoadConfig(filePath As String) As AppConfig
        Return JObject.Parse(File.ReadAllText(filePath)).ToObject(Of AppConfig)
    End Function

End Class
