﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Converters

Public Class UpdateInfo

    <JsonConverter(GetType(VersionConverter))>
    Public Property Version As Version
    Public ReadOnly Property Actions As New List(Of UpdateAction)

End Class
