Imports System.IO

Imports Newtonsoft.Json

Imports Pilz.Cryptography

Public Class UpdateConfig

    Public Property UpdateUrl As String
    Public Property WebdavURL As SecureString
    Public Property WebdavUsername As SecureString
    Public Property WebdavPassword As SecureString

    Public Sub SaveToFile(filePath As String)
        File.WriteAllText(filePath, JsonConvert.SerializeObject(Me))
    End Sub

    Public Shared Function LoadFromFile(filePath As String) As UpdateConfig
        Return JsonConvert.DeserializeObject(Of UpdateConfig)(File.ReadAllText(filePath))
    End Function

End Class
