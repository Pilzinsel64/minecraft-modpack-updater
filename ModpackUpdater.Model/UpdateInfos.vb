﻿Imports Newtonsoft.Json

Public Class UpdateInfos

    Public ReadOnly Property Updates As New List(Of UpdateInfo)

    Public Shared Function Parse(content As String) As UpdateInfos
        Return JsonConvert.DeserializeObject(Of UpdateInfos)(content)
    End Function

End Class
