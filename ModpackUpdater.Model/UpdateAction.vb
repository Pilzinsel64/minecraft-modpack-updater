﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Converters

Public Class UpdateAction

    <JsonConverter(GetType(StringEnumConverter))>
    Public Property Type As UpdateActionType
    Public Property DestPath As String
    Public Property SrcPath As String
    Public Property DownloadUrl As String
    Public Property IsDirectory As Boolean

End Class
