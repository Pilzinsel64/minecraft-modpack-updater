﻿Imports System.IO

Imports Newtonsoft.Json
Imports Newtonsoft.Json.Converters

Public Class ModpackInfo

    Private Const FILENAME_MODPACKINFO = "modpack-info.json"

    <JsonConverter(GetType(VersionConverter))>
    Public Property Version As Version

    Public Sub Save(mcRoot As String)
        File.WriteAllText(GetFilePath(mcRoot), JsonConvert.SerializeObject(Me))
    End Sub

    Public Shared Function Load(mcRoot As String) As ModpackInfo
        Return JsonConvert.DeserializeObject(Of ModpackInfo)(File.ReadAllText(GetFilePath(mcRoot)))
    End Function

    Public Shared Function HasModpackInfo(mcRoot As String) As Boolean
        Return File.Exists(GetFilePath(mcRoot))
    End Function

    Private Shared Function GetFilePath(mcRoot As String)
        Return Path.Combine(mcRoot, FILENAME_MODPACKINFO)
    End Function

End Class
