﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Converters

<JsonConverter(GetType(StringEnumConverter))>
<Flags>
Public Enum UpdateSyncAction
    None
    NewFile
    RemovedFile
    UpdatedFile = 4
End Enum
