﻿Imports ModpackUpdater.Model

Public Class UpdateCheckResult

    Public Property IsLegacy As Boolean
    Public ReadOnly Property SyncFiles As New List(Of UpdateSyncFile)

    Public Property CurrentVersion As Version
    Public Property LatestVersion As Version
    Public ReadOnly Property UpdateActions As New List(Of UpdateAction)
    Public Property HasError As Boolean

    Public ReadOnly Property HasUpdates As Boolean
        Get
            If IsLegacy Then
                Return SyncFiles.Where(Function(n) n.SyncAction <> UpdateSyncAction.None).Any
            Else
                Return CurrentVersion < LatestVersion
            End If
        End Get
    End Property

End Class
