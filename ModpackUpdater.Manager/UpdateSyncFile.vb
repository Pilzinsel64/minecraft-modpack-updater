Public Class UpdateSyncFile

    Public ReadOnly Property SyncAction As UpdateSyncAction
    Public ReadOnly Property LocalFile As string
    Public ReadOnly Property RemoveFile As string

    Public Sub New(syncAction As UpdateSyncAction, localFile As string, remoteFile As string)
        Me.SyncAction = syncAction
        Me.LocalFile = localFile
        Me.RemoveFile = remoteFile
    End Sub

End Class
